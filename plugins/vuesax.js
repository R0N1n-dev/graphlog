import Vue from "vue";
import Vuesax from "vuesax";
import "vuesax/dist/vuesax.css";
import "boxicons/css/boxicons.css";

Vue.use(Vuesax);
