import { GraphQLClient } from "graphql-request";

const graphcmsClient = new GraphQLClient("https://api-eu-central-1.hygraph.com/v2/cl2ft3azo1scq01xmf50ie858/master");

export default (_, inject) => {
  inject("graphcms", graphcmsClient);
};


