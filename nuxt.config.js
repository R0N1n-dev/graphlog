require("dotenv").config();
export default {
  target: "static",
  ssr: false,
  css: [],
  modules: ["@nuxtjs/dotenv"],
  buildModules: ["nuxt-windicss"],
  plugins: ["~/plugins/graphcms.js", "~/plugins/vuesax"],
  components: true,
  loading: false, // Disables the page change loader on the top
  head: {
    htmlAttrs: {
      lang: "en",
    },

    titleTemplate: "%s - My Blog",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
    ],
  },
  build: {},
  loading: false,
};
